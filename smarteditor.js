(function ($) {

Drupal.behaviors.initSmartEditor = {
  attach: function (context, settings) {

    // Add smart editor to body textareas with full_html text formats
    // on node edit or creation page.
    // 편집 화면 로드 시 full_html로 지정된 텍스트영역에 스마트에디터 입힌다.
    $('.filter-list', context).once('se2-check', function() {
      var targetarea = $(this).parents('.text-format-wrapper').find('.text-full');
      if ($(this).val() == 'full_html') se2init(targetarea);
    }).change(function(event){
      // Toggle loading smart editor per text format selected.
      // 텍스트 양식 선택에 따라 에디터 붙히고 지우기
      var container = $(event.target).parents('.text-format-wrapper');
      if ($(this).val() == 'full_html') {
        se2init(container.find('.text-full'));
      }
      else {
        container.find('.form-type-textarea iframe').remove();
        container.find('.text-full').show();
      }
    });
  },

};

// Actual initialization and send value to actual drupal form element
// before form submission.
// 스마트에디터를 대상 요소에 적용하고 저장 혹은 미리보기 시 값을 드루팔 텍스트영역으로 전달한다.
function se2init(target) {
  var oEditors = [];
  nhn.husky.EZCreator.createInIFrame({
    oAppRef: oEditors,
    elPlaceHolder: target.attr('id'),
    sSkinURI:"/sites/all/libraries/SE2/SmartEditor2Skin.html",
    fCreator: "createSEditor2",
    fOnAppLoad : function(){
      $('#edit-submit, #edit-preview').click(function(){
        oEditors.getById[target.attr('id')].exec("UPDATE_CONTENTS_FIELD", []);
      })
    },
  });
  $(".grippie").css("display", "none");
}


})(jQuery);
