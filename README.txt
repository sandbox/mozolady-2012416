Smart Editor Integration
------------------------
This module will replace node's body textarea fields with the Smart Editor,
another wysiwyg editor which is very familiar to korean. When you edit or
create node and selecct "Full html" text format, body textarea will be replaced
to smart editor. There is no configuration, no requiremenet check. ^^;;;
As simple but incomplete code, Use it at your own risk.


Installation
------------------------------------------------------------------------------
1. Download smart editor at http://dev.naver.com/projects/smarteditor/download
   Tested with SmartEditor2.0 Basic (2.3.1).
2. Extract it, locate to sites/all/libraries/SE2/(SmartEditor2.html and all files)
3. Install same as normal drupal modules and enable it.


Notes
------------------------------------------------------------------------------
You can upload image by clicking picture icon of smart editor.
For quick image upload to work, some code changes needs to be done.
This will make your files located to smarteditor directory under default files
directory.

Location
--------
/sites/all/libraries/SE2/photo_uploader

Files to edit
-------------
file_uploader_html5.php
line 17: $uploadDir = '../../../../../default/files/smarteditor/';
line 27: $sFileInfo .= "&sFileURL=/sites/default/files/smarteditor/".$file->name;

file_uploader.php
line 13: $uploadDir = '../../../../../default/files/smarteditor/';
line 24: $sFileInfo .= "&sFileURL=/sites/default/files/smarteditor/".$file->name;


